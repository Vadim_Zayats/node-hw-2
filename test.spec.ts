import { Card, CurrencyEnum, Transaction } from "./index";

describe("class Card`s methods tests", () => {
  let card: Card;
  let trans1: Transaction;
  let trans2: Transaction;
  //   let trans3: Transaction;
  //   let trans4: Transaction;

  beforeEach(() => {
    card = new Card();

    trans1 = new Transaction(100, CurrencyEnum.USD);
    trans2 = new Transaction(100, CurrencyEnum.USD);
    // trans3 = new Transaction(200, CurrencyEnum.UAH);
    // trans4 = new Transaction(200, CurrencyEnum.UAH);

    card.addTransaction(trans1);
    card.addTransaction(trans2);
    // card.addTransaction(trans3);
    // card.addTransaction(trans4);
  });

  test("getTransaction", () => {
    const retrievedTransaction = card.getTransaction(trans1.id);
    const retrievedTransaction2 = card.getTransaction(trans2.id);

    expect(retrievedTransaction).toEqual(trans1);
    expect(retrievedTransaction2).toEqual(trans2);
  });

  test("getBalance", () => {
    const balanceUAH = card.getBalance(CurrencyEnum.UAH);
    const balanceUSD = card.getBalance(CurrencyEnum.USD);

    expect(balanceUAH).toBe(0);
    // expect(balanceUAH).toBe(400);
    expect(balanceUSD).toBe(200);
  });
});
